<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dash_overall_visitors'] = 'Overall Visitors';
$lang['dash_filter_by_site'] = 'Filter by Site';

$lang['dash_year'] = 'Year';
$lang['dash_month'] = 'Month';
$lang['dash_chat_requests_per_day'] = 'Chat Requests Per Day';
$lang['dash_visitors_per_day'] = 'Visitors Per Day';

$lang['zoom_and_clear'] = 'To zoom in, select the area and to reset click on <a href="#" ng-click="claerChart($event)">CLEAR</a>';
$lang['dash_no_record_found'] = 'No record found';

$lang['dash_overall_requests'] = 'Overall Requests';
$lang['dash_online_requests'] = 'Online Requests';
$lang['dash_offline_requests'] = 'Offline Requests';
$lang['dash_launch_chat_panel'] = 'Launch Chat Panel';

$lang['dash_most_rated_operators'] = 'Most Rated Agents';

$lang['title'] = 'Title';
$lang['visits'] = 'Visits';
$lang['visitors_map'] = 'Visitors Map';
$lang['recent_anonymous_visitors'] = 'Online Anonymous Visitors';

$lang['dash_all'] = 'All';
$lang['dash_online_visitors'] = 'Online Visitors';
$lang['dash_attended_visitors'] = 'Attended Visitors';

$lang['dash_operators_and_requests'] = 'Agents and Requests';
$lang['dash_most_visited_pages'] = 'Most Visited Pages';
$lang['dash_recent_chats'] = 'Recents Chats';
$lang['dash_filter_by'] = 'Filter by';

