-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2018 at 08:22 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `livechat`
--

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_anonymous_messages`
--

CREATE TABLE `chatbull_anonymous_messages` (
  `id` int(11) UNSIGNED NOT NULL,
  `temp_visitor_id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `local_id` varchar(255) NOT NULL,
  `sort_order` varchar(255) NOT NULL,
  `chat_message` mediumtext NOT NULL,
  `message_status` enum('read','unread') NOT NULL DEFAULT 'unread',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_canned_messages`
--

CREATE TABLE `chatbull_canned_messages` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_chat_messages`
--

CREATE TABLE `chatbull_chat_messages` (
  `id` int(11) UNSIGNED NOT NULL,
  `chat_session_id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `local_id` varchar(25) NOT NULL,
  `sort_order` varchar(20) NOT NULL,
  `chat_message` mediumtext NOT NULL,
  `message_meta` text,
  `message_type` varchar(30) DEFAULT 'text',
  `message_status` enum('read','unread') NOT NULL DEFAULT 'unread',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_chat_requests`
--

CREATE TABLE `chatbull_chat_requests` (
  `id` int(11) UNSIGNED NOT NULL,
  `chat_session_id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `requested_to` int(11) UNSIGNED NOT NULL,
  `requested_by` int(11) UNSIGNED DEFAULT '0',
  `request_type` enum('new','forward') NOT NULL DEFAULT 'new',
  `message` mediumtext NOT NULL,
  `request_status` enum('pending','accepted','cancelled','closed') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_chat_sessions`
--

CREATE TABLE `chatbull_chat_sessions` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0',
  `user_agent` text,
  `port` varchar(11) NOT NULL DEFAULT '0',
  `requested_tag` int(11) DEFAULT '0',
  `supported_tags` mediumtext,
  `session_status` enum('requested','open','forward','closed','disconnected','on-hold') NOT NULL DEFAULT 'requested',
  `session_type` enum('public','private') NOT NULL DEFAULT 'public'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_chat_users`
--

CREATE TABLE `chatbull_chat_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `chat_session_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `address_id` int(11) UNSIGNED DEFAULT '0',
  `user_role` enum('agent','visitor') NOT NULL DEFAULT 'visitor',
  `is_typing` char(1) NOT NULL DEFAULT '0',
  `user_state` enum('in-chat','left') NOT NULL DEFAULT 'in-chat',
  `started_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_configuration`
--

CREATE TABLE `chatbull_configuration` (
  `id` int(11) UNSIGNED NOT NULL,
  `config_option` varchar(255) NOT NULL DEFAULT '',
  `config_value` mediumtext NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chatbull_configuration`
--

INSERT INTO `chatbull_configuration` (`id`, `config_option`, `config_value`, `site_id`) VALUES
(1, 'licence_key', '0840dab3-54a8-4bcb-a4de-d00dd5775861', 0),
(2, 'google_map_api_key', '', 0),
(3, 'google_map_zoom', '2', 0),
(4, 'notify_all_requests_admin', 'no', 0),
(5, 'enable_agent_initiate_chats', 'yes', 0),
(6, 'enable_canned_messages', 'yes', 0),
(7, 'enable_agent_file_sharing', 'yes', 0),
(8, 'agent_time_interwal', '3', 0),
(9, 'agent_file_upload_size', '5000', 0),
(10, 'agent_allowed_filetypes', '.docx|.txt|.gif|.jpg|.png', 0),
(11, 'admin_panel_logo', '', 0),
(12, 'admin_panel_name', 'ChatBull', 0),
(13, 'admin_panel_email', 'akash@wistech.biz', 0),
(14, 'operator_chat_theme', 'bubbles-boom', 0),
(15, 'current_version', '5.1.7', 0),
(16, 'current_product_name', 'chatbull', 0),
(17, 'site_lived_year', '2018-11-19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_daily_pageviews`
--

CREATE TABLE `chatbull_daily_pageviews` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0',
  `counter` int(11) UNSIGNED NOT NULL,
  `page_title` varchar(150) NOT NULL DEFAULT '',
  `page_url` text NOT NULL,
  `created_at` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_daily_visitors`
--

CREATE TABLE `chatbull_daily_visitors` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0',
  `visitors` int(11) UNSIGNED NOT NULL,
  `created_at` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_feedback`
--

CREATE TABLE `chatbull_feedback` (
  `id` int(11) UNSIGNED NOT NULL,
  `chat_session_id` int(11) UNSIGNED NOT NULL,
  `feedback_by` int(11) UNSIGNED NOT NULL,
  `feedback_to` int(11) UNSIGNED NOT NULL,
  `rating` int(11) UNSIGNED NOT NULL,
  `feedback_text` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_gcm_users`
--

CREATE TABLE `chatbull_gcm_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `mac_address` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `user_status` char(1) NOT NULL DEFAULT '1',
  `device_type` enum('iOS','android') NOT NULL DEFAULT 'android',
  `device_id` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_languages`
--

CREATE TABLE `chatbull_languages` (
  `id` int(11) UNSIGNED NOT NULL,
  `lang_name` varchar(100) NOT NULL DEFAULT '',
  `machine_name` varchar(100) NOT NULL DEFAULT '',
  `lang_flag` varchar(255) NOT NULL DEFAULT '',
  `lang_status` enum('publish','unpublish') NOT NULL DEFAULT 'unpublish',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chatbull_languages`
--

INSERT INTO `chatbull_languages` (`id`, `lang_name`, `machine_name`, `lang_flag`, `lang_status`, `created_at`, `modified_at`) VALUES
(1, 'English', 'english', '', 'publish', '2018-11-19 03:07:38', '2018-11-19 03:07:38');

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_migrations`
--

CREATE TABLE `chatbull_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chatbull_migrations`
--

INSERT INTO `chatbull_migrations` (`version`) VALUES
(20170915150855);

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_notifications`
--

CREATE TABLE `chatbull_notifications` (
  `id` int(11) UNSIGNED NOT NULL,
  `chat_session_id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED DEFAULT '0',
  `request_id` int(11) UNSIGNED DEFAULT '0',
  `message` varchar(255) NOT NULL,
  `display_message` text NOT NULL,
  `notification_type` enum('message','online_request','offline_request','offline_reply') NOT NULL DEFAULT 'message',
  `notification_status` enum('read','unread') NOT NULL DEFAULT 'unread',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_offline_requests`
--

CREATE TABLE `chatbull_offline_requests` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0',
  `visitor_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `address_id` int(11) UNSIGNED DEFAULT '0',
  `visitor_note` mediumtext NOT NULL,
  `request_status` enum('pending','open','closed') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_offline_requests_conversations`
--

CREATE TABLE `chatbull_offline_requests_conversations` (
  `id` int(11) UNSIGNED NOT NULL,
  `request_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `sender_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `message` mediumtext NOT NULL,
  `message_status` enum('read','unread') NOT NULL DEFAULT 'unread',
  `conversation_type` enum('query','reply') NOT NULL DEFAULT 'query',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_offline_request_tags`
--

CREATE TABLE `chatbull_offline_request_tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `orequest_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_password_reminders`
--

CREATE TABLE `chatbull_password_reminders` (
  `email` varchar(100) NOT NULL DEFAULT '',
  `token` varchar(25) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_session`
--

CREATE TABLE `chatbull_session` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '',
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chatbull_session`
--

INSERT INTO `chatbull_session` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('7vethkfuios2i66s1hlmip6v476s1p2u', '127.0.0.1', 1542611786, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534323631313737303b63757272656e745f757365725f69647c733a313a2231223b6469736d69735f7570646174655f616c6572747c733a323a226e6f223b),
('mt3pausk137d1ph0252g215upoek1bk3', '127.0.0.1', 1542611770, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534323631313737303b63757272656e745f757365725f69647c733a313a2231223b6469736d69735f7570646174655f616c6572747c733a323a226e6f223b);

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_sites`
--

CREATE TABLE `chatbull_sites` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_name` varchar(150) DEFAULT '',
  `site_email` varchar(100) DEFAULT '',
  `site_logo` varchar(255) DEFAULT '',
  `site_url` text NOT NULL,
  `token` tinytext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_tags`
--

CREATE TABLE `chatbull_tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `tag_name` varchar(100) NOT NULL DEFAULT '',
  `tag_status` enum('publish','unpublish') NOT NULL DEFAULT 'publish'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_temp_visitors`
--

CREATE TABLE `chatbull_temp_visitors` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED NOT NULL,
  `operator_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `chat_session_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `message` mediumtext NOT NULL,
  `profile_color` varchar(10) NOT NULL DEFAULT '',
  `ip_address` varchar(70) NOT NULL DEFAULT '',
  `page_url` text NOT NULL,
  `page_title` varchar(150) NOT NULL DEFAULT '',
  `meta_data` text,
  `latitude` decimal(10,7) NOT NULL,
  `longitude` decimal(10,7) NOT NULL,
  `city` varchar(150) NOT NULL DEFAULT '',
  `state` varchar(150) NOT NULL DEFAULT '',
  `country` varchar(150) NOT NULL DEFAULT '',
  `status` enum('present','leaved') NOT NULL DEFAULT 'leaved',
  `last_activity_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_users`
--

CREATE TABLE `chatbull_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `display_name` varchar(50) NOT NULL DEFAULT '',
  `contact_number` varchar(25) NOT NULL DEFAULT '',
  `pass` varchar(100) NOT NULL DEFAULT '',
  `profile_pic` varchar(255) NOT NULL DEFAULT '',
  `profile_color` varchar(10) DEFAULT '',
  `user_status` enum('blocked','active','deleted') NOT NULL DEFAULT 'active',
  `role` enum('admin','agent','visitor') NOT NULL DEFAULT 'visitor',
  `remember_token` varchar(25) NOT NULL DEFAULT '',
  `last_activity_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mobile_last_activity_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `desktop_last_activity_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chatbull_users`
--

INSERT INTO `chatbull_users` (`id`, `name`, `email`, `display_name`, `contact_number`, `pass`, `profile_pic`, `profile_color`, `user_status`, `role`, `remember_token`, `last_activity_time`, `mobile_last_activity_time`, `desktop_last_activity_time`, `last_login`, `created_at`, `modified_at`) VALUES
(1, 'Akash', 'akash@wistech.biz', 'Akash', '', 'e10adc3949ba59abbe56e057f20f883e', '', '#aed581', 'active', 'admin', 'uRxyEe46mBjr5NPVlGX7', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-11-19 03:08:04', '2018-11-19 03:07:38', '2018-11-19 03:07:38');

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_users_to_sites`
--

CREATE TABLE `chatbull_users_to_sites` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_user_address`
--

CREATE TABLE `chatbull_user_address` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0',
  `user_id` int(11) UNSIGNED NOT NULL,
  `latitude` decimal(10,7) NOT NULL,
  `longitude` decimal(10,7) NOT NULL,
  `city` varchar(150) NOT NULL DEFAULT '',
  `state` varchar(150) NOT NULL DEFAULT '',
  `country` varchar(150) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_user_tags`
--

CREATE TABLE `chatbull_user_tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chatbull_user_visit_info`
--

CREATE TABLE `chatbull_user_visit_info` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0',
  `meta_data` text,
  `user_id` int(11) UNSIGNED NOT NULL,
  `request_id` int(11) UNSIGNED NOT NULL,
  `request_type` enum('online','offline') NOT NULL DEFAULT 'online',
  `ip_address` varchar(70) NOT NULL DEFAULT '',
  `page_url` text NOT NULL,
  `page_title` varchar(150) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chatbull_anonymous_messages`
--
ALTER TABLE `chatbull_anonymous_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `temp_visitor_id` (`temp_visitor_id`);

--
-- Indexes for table `chatbull_canned_messages`
--
ALTER TABLE `chatbull_canned_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `chatbull_chat_messages`
--
ALTER TABLE `chatbull_chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `chat_session_id_sender_id` (`chat_session_id`,`sender_id`),
  ADD KEY `local_id` (`local_id`);

--
-- Indexes for table `chatbull_chat_requests`
--
ALTER TABLE `chatbull_chat_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `chat_session_id_sender_id` (`chat_session_id`,`sender_id`),
  ADD KEY `requested_to_requested_by` (`requested_to`,`requested_by`);

--
-- Indexes for table `chatbull_chat_sessions`
--
ALTER TABLE `chatbull_chat_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_chat_users`
--
ALTER TABLE `chatbull_chat_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `chat_session_id_user_id` (`chat_session_id`,`user_id`),
  ADD KEY `address_id` (`address_id`);

--
-- Indexes for table `chatbull_configuration`
--
ALTER TABLE `chatbull_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_daily_pageviews`
--
ALTER TABLE `chatbull_daily_pageviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_daily_visitors`
--
ALTER TABLE `chatbull_daily_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_feedback`
--
ALTER TABLE `chatbull_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feedback_by` (`feedback_by`),
  ADD KEY `chat_session_id_feedback_by` (`chat_session_id`,`feedback_by`),
  ADD KEY `feedback_to` (`feedback_to`);

--
-- Indexes for table `chatbull_gcm_users`
--
ALTER TABLE `chatbull_gcm_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `chatbull_languages`
--
ALTER TABLE `chatbull_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_notifications`
--
ALTER TABLE `chatbull_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `chat_session_id_sender_id` (`chat_session_id`,`sender_id`),
  ADD KEY `receiver_id_request_id` (`receiver_id`,`request_id`);

--
-- Indexes for table `chatbull_offline_requests`
--
ALTER TABLE `chatbull_offline_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visitor_id_address_id` (`visitor_id`,`address_id`);

--
-- Indexes for table `chatbull_offline_requests_conversations`
--
ALTER TABLE `chatbull_offline_requests_conversations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_id_sender_id` (`request_id`,`sender_id`);

--
-- Indexes for table `chatbull_offline_request_tags`
--
ALTER TABLE `chatbull_offline_request_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `orequest_id_tag_id` (`orequest_id`,`tag_id`);

--
-- Indexes for table `chatbull_password_reminders`
--
ALTER TABLE `chatbull_password_reminders`
  ADD KEY `email` (`email`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `chatbull_session`
--
ALTER TABLE `chatbull_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `chatbull_sites`
--
ALTER TABLE `chatbull_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_tags`
--
ALTER TABLE `chatbull_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_temp_visitors`
--
ALTER TABLE `chatbull_temp_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_users`
--
ALTER TABLE `chatbull_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbull_users_to_sites`
--
ALTER TABLE `chatbull_users_to_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `user_id_site_id` (`user_id`,`site_id`);

--
-- Indexes for table `chatbull_user_address`
--
ALTER TABLE `chatbull_user_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `chatbull_user_tags`
--
ALTER TABLE `chatbull_user_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `user_id_tag_id` (`user_id`,`tag_id`);

--
-- Indexes for table `chatbull_user_visit_info`
--
ALTER TABLE `chatbull_user_visit_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_request_id` (`user_id`,`request_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chatbull_anonymous_messages`
--
ALTER TABLE `chatbull_anonymous_messages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_canned_messages`
--
ALTER TABLE `chatbull_canned_messages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_chat_messages`
--
ALTER TABLE `chatbull_chat_messages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_chat_requests`
--
ALTER TABLE `chatbull_chat_requests`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_chat_sessions`
--
ALTER TABLE `chatbull_chat_sessions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_chat_users`
--
ALTER TABLE `chatbull_chat_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_configuration`
--
ALTER TABLE `chatbull_configuration`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `chatbull_daily_pageviews`
--
ALTER TABLE `chatbull_daily_pageviews`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_daily_visitors`
--
ALTER TABLE `chatbull_daily_visitors`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_feedback`
--
ALTER TABLE `chatbull_feedback`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_gcm_users`
--
ALTER TABLE `chatbull_gcm_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_languages`
--
ALTER TABLE `chatbull_languages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chatbull_notifications`
--
ALTER TABLE `chatbull_notifications`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_offline_requests`
--
ALTER TABLE `chatbull_offline_requests`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_offline_requests_conversations`
--
ALTER TABLE `chatbull_offline_requests_conversations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_offline_request_tags`
--
ALTER TABLE `chatbull_offline_request_tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_sites`
--
ALTER TABLE `chatbull_sites`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_tags`
--
ALTER TABLE `chatbull_tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_temp_visitors`
--
ALTER TABLE `chatbull_temp_visitors`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_users`
--
ALTER TABLE `chatbull_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chatbull_users_to_sites`
--
ALTER TABLE `chatbull_users_to_sites`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_user_address`
--
ALTER TABLE `chatbull_user_address`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_user_tags`
--
ALTER TABLE `chatbull_user_tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatbull_user_visit_info`
--
ALTER TABLE `chatbull_user_visit_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chatbull_anonymous_messages`
--
ALTER TABLE `chatbull_anonymous_messages`
  ADD CONSTRAINT `chatbull_anonymous_messages_ibfk_1` FOREIGN KEY (`temp_visitor_id`) REFERENCES `chatbull_temp_visitors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_canned_messages`
--
ALTER TABLE `chatbull_canned_messages`
  ADD CONSTRAINT `chatbull_canned_messages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_chat_messages`
--
ALTER TABLE `chatbull_chat_messages`
  ADD CONSTRAINT `chatbull_chat_messages_ibfk_1` FOREIGN KEY (`chat_session_id`) REFERENCES `chatbull_chat_sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_chat_messages_ibfk_2` FOREIGN KEY (`sender_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_chat_requests`
--
ALTER TABLE `chatbull_chat_requests`
  ADD CONSTRAINT `chatbull_chat_requests_ibfk_1` FOREIGN KEY (`chat_session_id`) REFERENCES `chatbull_chat_sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_chat_requests_ibfk_2` FOREIGN KEY (`sender_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_chat_requests_ibfk_3` FOREIGN KEY (`requested_to`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_chat_users`
--
ALTER TABLE `chatbull_chat_users`
  ADD CONSTRAINT `chatbull_chat_users_ibfk_1` FOREIGN KEY (`chat_session_id`) REFERENCES `chatbull_chat_sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_chat_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_feedback`
--
ALTER TABLE `chatbull_feedback`
  ADD CONSTRAINT `chatbull_feedback_ibfk_1` FOREIGN KEY (`chat_session_id`) REFERENCES `chatbull_chat_sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_feedback_ibfk_2` FOREIGN KEY (`feedback_by`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_feedback_ibfk_3` FOREIGN KEY (`feedback_to`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_gcm_users`
--
ALTER TABLE `chatbull_gcm_users`
  ADD CONSTRAINT `chatbull_gcm_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_notifications`
--
ALTER TABLE `chatbull_notifications`
  ADD CONSTRAINT `chatbull_notifications_ibfk_1` FOREIGN KEY (`chat_session_id`) REFERENCES `chatbull_chat_sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_notifications_ibfk_2` FOREIGN KEY (`sender_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_notifications_ibfk_3` FOREIGN KEY (`receiver_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_offline_requests_conversations`
--
ALTER TABLE `chatbull_offline_requests_conversations`
  ADD CONSTRAINT `chatbull_offline_requests_conversations_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `chatbull_offline_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_offline_request_tags`
--
ALTER TABLE `chatbull_offline_request_tags`
  ADD CONSTRAINT `chatbull_offline_request_tags_ibfk_1` FOREIGN KEY (`orequest_id`) REFERENCES `chatbull_offline_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_offline_request_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `chatbull_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_users_to_sites`
--
ALTER TABLE `chatbull_users_to_sites`
  ADD CONSTRAINT `chatbull_users_to_sites_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_users_to_sites_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `chatbull_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_user_address`
--
ALTER TABLE `chatbull_user_address`
  ADD CONSTRAINT `chatbull_user_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_user_tags`
--
ALTER TABLE `chatbull_user_tags`
  ADD CONSTRAINT `chatbull_user_tags_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatbull_user_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `chatbull_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatbull_user_visit_info`
--
ALTER TABLE `chatbull_user_visit_info`
  ADD CONSTRAINT `chatbull_user_visit_info_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `chatbull_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
